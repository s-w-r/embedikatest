ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.3.0"

lazy val root = (project in file("."))
  .settings(
    name := "EmbedikaTest"
  )

val http4sVersion = "0.23.23"
val catsCoreVersion = "2.9.0"
val catsEffectVersion = "3.5.1"
val doobieVersion = "1.0.0-RC4"
val tapirVersion = "1.6.4"
val circeVersion = "0.14.5"
val log4catsVersion = "2.6.0"
val flywaydbVersion = "9.16.0"
val pureConfigVersion = "0.17.3"
val typesafeVersion = "1.4.2"
val apiSpecVersion = "0.6.0"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % catsCoreVersion,
  "org.typelevel" %% "cats-effect" % catsEffectVersion,

  "org.typelevel" %% "log4cats-core" % log4catsVersion,
  "org.typelevel" %% "log4cats-slf4j" % log4catsVersion,
  "org.slf4j" % "slf4j-simple" % "2.0.5",
  "org.slf4j" % "slf4j-api" % "2.0.5",

  "com.typesafe" % "config" % typesafeVersion,

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  "com.github.losizm" %% "little-config" % "4.0.0",

  "org.tpolecat" %% "doobie-core" % doobieVersion,
  "org.tpolecat" %% "doobie-specs2"   % doobieVersion,
  "org.tpolecat" %% "doobie-postgres" % doobieVersion,
  "org.tpolecat" %% "doobie-postgres-circe" % doobieVersion,

  "org.http4s" %% "http4s-ember-client" % http4sVersion,
  "org.http4s" %% "http4s-ember-server" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,

  "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-core" % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % tapirVersion,

  "com.softwaremill.sttp.apispec" %% "openapi-circe-yaml" % apiSpecVersion,

  "org.flywaydb" % "flyway-core" % flywaydbVersion
)

//addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

