package docs

import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp
import cats.effect.std.Console

import io.circe.Printer
import io.circe.syntax.*
import server.Endpoints
import sttp.apispec.openapi.OpenAPI
import sttp.apispec.openapi.circe._
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter


object DocsGenerator extends IOApp{
  override def run(args: List[String]): IO[ExitCode] = {
    val endpoints = Endpoints.getCarEndpoint :: Endpoints.getCarsListEndpoint :: Endpoints.putCarEndpoint ::  Endpoints.deleteCarEndpoint :: Endpoints.getStatisticEndpoint :: Nil
    val docs: OpenAPI = OpenAPIDocsInterpreter().toOpenAPI(endpoints, "My Bookshop", "1.0").openapi("3.0.3")

    Console[IO].println(Printer.spaces2.print(docs.asJson)).map(_ => ExitCode.Success)
  }
}
