package jdbc

import cats.effect.IO

import configuration.JdbcDatabaseConfig
import doobie.Transactor
import doobie.*

object DBTransactor {
  def instance(implicit jdbcDatabaseConfig: JdbcDatabaseConfig): Transactor[IO] = doobie.Transactor.fromDriverManager[IO](
    driver = jdbcDatabaseConfig.driver,
    url = jdbcDatabaseConfig.url,
    user = jdbcDatabaseConfig.user,
    password = jdbcDatabaseConfig.password,
    logHandler = None
  )
}
