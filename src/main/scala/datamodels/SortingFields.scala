package datamodels

object SortingFields{
  sealed trait SortingField

  case object CreatedAt extends SortingField

  case object ManufacturingDate extends SortingField

  case object CarNumber extends SortingField

  case object CarType extends SortingField
}



