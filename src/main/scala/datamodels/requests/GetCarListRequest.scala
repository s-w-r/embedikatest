package datamodels.requests

import datamodels.SortingFields.SortingField

final case class GetCarListRequest(limit: Option[Int], offset: Option[Int], sortingFields: Option[SortingField])

