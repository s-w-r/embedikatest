package datamodels.requests

import java.time.LocalDate
import java.util.UUID

import io.circe.generic.auto.*

//uuid как гарантия идемпотентности, теоретически можно было сделать номер как pk, но звучит так себе
final case class PutCarRequest(id: UUID, carType: String, number: String, color: String, manufacturingDate: LocalDate)