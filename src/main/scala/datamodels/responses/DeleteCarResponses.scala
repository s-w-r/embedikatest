package datamodels.responses

import java.util.UUID

object DeleteCarResponses {
  sealed trait DeleteCarError
  final case class CarDontExist(id: UUID) extends DeleteCarError
}
