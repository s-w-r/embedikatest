package datamodels.responses

import java.util.UUID

import io.circe.generic.auto.*

object GetCarsStatisticResponses{
  final case class CarsStatistic(count: Int, oldestCarId: UUID, newestCarId: UUID)
}