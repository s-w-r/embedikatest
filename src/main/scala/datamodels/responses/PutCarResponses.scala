package datamodels.responses

object PutCarResponses {
  sealed trait PutCarError
  case object CarAlreadyExists extends PutCarError
}
