package datamodels

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.UUID

// estatico newtypes не завезли в scala3, opaque types - неудобны, поэтому используем примитивы
final case class Car(id: UUID, carType: String, number: String, color: String, manufacturingDate: LocalDate, createdAt: LocalDateTime)