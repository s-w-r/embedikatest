package datamodels.validators
import io.circe.generic.auto.*

final case class RequestValidationError(description: List[String])
