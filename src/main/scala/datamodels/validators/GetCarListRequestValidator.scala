package datamodels.validators

import cats.data.Validated
import cats.data.ValidatedNel

import datamodels.requests.GetCarListRequest

object GetCarListRequestValidator {
  private def validateLimit(request: GetCarListRequest): ValidatedNel[String, GetCarListRequest] = Validated.cond(
    request.limit.forall(_ > 0),
    request,
    s"Limit must be more than 0"
  ).toValidatedNel

  private def validateOffset(request: GetCarListRequest): ValidatedNel[String, GetCarListRequest] = Validated.cond(
    request.offset.forall(_ >= 0),
    request,
    s"Offset must be more or equal 0"
  ).toValidatedNel

  def validatePutCarItemRequest(request: GetCarListRequest): ValidatedNel[String, GetCarListRequest] =
    validateLimit(request) andThen  validateOffset
}
