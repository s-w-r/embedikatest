package datamodels.validators

import cats.data.Validated
import cats.data.ValidatedNel

import datamodels.requests.PutCarRequest

object PutCarRequestValidator {
  private val numberRegex = "^\\w\\d\\d\\d\\w\\w\\d\\d$".r

  private def validateNumber(request: PutCarRequest): Validated[String, PutCarRequest] = Validated.cond(
    numberRegex.matches(request.number),
    request,
    s"Incorrect car number format. expected: a000aa00, found: ${request.number}"
  )

  def validatePutCarItemRequest(request: PutCarRequest): ValidatedNel[String, PutCarRequest] = validateNumber(request).toValidatedNel
}
