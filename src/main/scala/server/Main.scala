package server

import scala.concurrent.ExecutionContext

import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp
import cats.effect.Sync

import com.comcast.ip4s.*
import configuration.JdbcDatabaseConfig
import doobie.Transactor
import jdbc.DBMigrations
import jdbc.DBTransactor
import org.http4s.ember.server.*
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import repositories.CarRepository
import services.CarService

object Main extends IOApp {

  implicit def logger[F[_] : Sync]: Logger[F] = Slf4jLogger.getLogger[F]

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  override def run(args: List[String]): IO[ExitCode] = for {
    given JdbcDatabaseConfig <- JdbcDatabaseConfig.loadFromGlobal[IO]("jdbc")
    _ <- DBMigrations.migrate[IO]
    given Transactor[IO] = DBTransactor.instance // implicit0 лучше
    carRepository = CarRepository.instance
    carService = CarService.instance(carRepository)
    httpApp = App.instance(carService)

    server <- EmberServerBuilder.default[IO]
      .withHost(ipv4"0.0.0.0")
      .withPort(port"8080")
      .withHttpApp(httpApp)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)
  } yield server
}
