package server

import cats.data.Validated.Invalid
import cats.data.Validated.Valid
import cats.effect.IO
import cats.syntax.all.*

import datamodels.requests.GetCarListRequest
import datamodels.requests.PutCarRequest
import datamodels.validators.GetCarListRequestValidator
import datamodels.validators.PutCarRequestValidator
import datamodels.validators.RequestValidationError
import org.http4s.HttpApp
import services.CarService
import sttp.capabilities.fs2.Fs2Streams
import sttp.tapir.server
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.http4s.Http4sServerInterpreter


object App {
  def instance(carService: CarService[IO]): HttpApp[IO] = {
    val endpoints: List[ServerEndpoint[Fs2Streams[IO], IO]] = List(
      Endpoints.getCarEndpoint.serverLogicSuccess(carService.getCar),

      Endpoints.getCarsListEndpoint.serverLogic(request => GetCarListRequestValidator.validatePutCarItemRequest(request) match {
        case Valid(r) => carService.getCarList(r).map(_.asRight)
        case Invalid(e) => RequestValidationError(e.toList).asLeft.pure[IO]
      }),

      Endpoints.putCarEndpoint.serverLogic(request => PutCarRequestValidator.validatePutCarItemRequest(request) match {
        case Valid(r) => carService.putCar(r)
        case Invalid(e) => RequestValidationError(e.toList).asLeft.pure[IO]
      }),

      Endpoints.deleteCarEndpoint.serverLogic(carService.deleteCar),

      Endpoints.getStatisticEndpoint.serverLogicSuccess(_ => carService.getStatistic)
    )

    Http4sServerInterpreter[IO]()
      .toRoutes(endpoints)
      .orNotFound
  }
}
