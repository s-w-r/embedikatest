package server

import java.util.UUID

import datamodels.Car
import datamodels.requests.GetCarListRequest
import datamodels.requests.PutCarRequest
import datamodels.responses.DeleteCarResponses.DeleteCarError
import datamodels.responses.GetCarsStatisticResponses.CarsStatistic
import datamodels.responses.PutCarResponses
import datamodels.responses.PutCarResponses.PutCarError
import datamodels.validators.RequestValidationError
import io.circe.generic.auto.*
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import sttp.tapir.json.circe.*

object Endpoints {
  val baseEndpointV1: PublicEndpoint[Unit, Unit, Unit, Any] =
    endpoint.in("cars" / "v1")

  val catalogBaseEndpoint: PublicEndpoint[Unit, Unit, Unit, Any] = baseEndpointV1.in("catalog")

  val getCarEndpoint: PublicEndpoint[UUID, Unit, Option[Car], Any] =
    catalogBaseEndpoint
      .get
      .in(path[UUID])
      .out(jsonBody[Option[Car]])

  val getCarsListEndpoint: PublicEndpoint[GetCarListRequest, RequestValidationError, List[Car], Any] =
    catalogBaseEndpoint
      .post
      .in("list")
      .in(jsonBody[GetCarListRequest])
      .errorOut(jsonBody[RequestValidationError])
      .out(jsonBody[List[Car]])

  val putCarEndpoint: PublicEndpoint[PutCarRequest, PutCarError | RequestValidationError, Unit, Any] =
    catalogBaseEndpoint
      .put
      .in(jsonBody[PutCarRequest])
      .errorOut(jsonBody[PutCarError])
      .errorOutVariant(oneOfVariant(jsonBody[RequestValidationError]))

  val deleteCarEndpoint: PublicEndpoint[UUID, DeleteCarError, Unit, Any] =
    catalogBaseEndpoint
      .delete
      .errorOut(jsonBody[DeleteCarError])
      .in(path[UUID])

  val getStatisticEndpoint: PublicEndpoint[Unit, Unit, CarsStatistic, Any] =
    baseEndpointV1
      .get
      .in("statistic")
      .out(jsonBody[CarsStatistic])

}
