package configuration

import scala.jdk.CollectionConverters.*

import cats.effect.Sync

import com.typesafe.config.ConfigFactory

final case class JdbcDatabaseConfig
(
  url: String,
  driver: String,
  user: String,
  password: String,
  migrationsLocations: List[String]
)

object JdbcDatabaseConfig {
  def loadFromGlobal[F[_] : Sync](configNamespace: String): F[JdbcDatabaseConfig] =
    Sync[F].blocking {
      val config = ConfigFactory.load().getConfig("jdbc")
      JdbcDatabaseConfig(
        config.getString("url"),
        config.getString("driver"),
        config.getString("user"),
        config.getString("password"),
        config.getStringList("migrationsLocations").asScala.toList
      )
    }
}