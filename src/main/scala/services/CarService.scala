package services

import java.time.LocalDateTime
import java.util.UUID

import scala.language.postfixOps

import cats.effect.Async
import cats.effect.IO

import datamodels.Car
import datamodels.requests.GetCarListRequest
import datamodels.requests.PutCarRequest
import datamodels.responses.DeleteCarResponses.DeleteCarError
import datamodels.responses.GetCarsStatisticResponses.CarsStatistic
import datamodels.responses.PutCarResponses.PutCarError
import org.typelevel.log4cats.Logger
import repositories.CarRepository

trait CarService[F[_] : Async : Logger] {
  def getCar(id: UUID): F[Option[Car]]

  def getCarList(getCarsRequest: GetCarListRequest): F[List[Car]]

  def putCar(putCarRequest: PutCarRequest): F[Either[PutCarError, Unit]]

  def deleteCar(id: UUID): F[Either[DeleteCarError, Unit]]

  def getStatistic: F[CarsStatistic]
}

object CarService {
  def instance(carRepo: CarRepository[IO])(implicit logger: Logger[IO]): CarService[IO] = new CarService[IO] {
    private val carRepository = carRepo

    def getCar(id: UUID): IO[Option[Car]] = carRepository.getCar(id)

    def getCarList(getCarsRequest: GetCarListRequest): IO[List[Car]] = carRepository.getCars(getCarsRequest)

    def putCar(putCarRequest: PutCarRequest): IO[Either[PutCarError, Unit]] = {
      val car = Car(putCarRequest.id,
        carType = putCarRequest.carType,
        number = putCarRequest.number,
        color = putCarRequest.color,
        manufacturingDate = putCarRequest.manufacturingDate,
        createdAt = LocalDateTime.now())
      carRepository.putCar(car)
    }

    def deleteCar(id: UUID): IO[Either[DeleteCarError, Unit]] = carRepository.deleteCar(id)

    def getStatistic: IO[CarsStatistic] = carRepository.getStatistic
  }
}
