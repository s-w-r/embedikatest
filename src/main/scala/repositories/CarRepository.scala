package repositories

import java.util.UUID

import cats.effect.IO
import cats.effect.Sync
import cats.syntax.all.*

import datamodels.Car
import datamodels.SortingFields
import datamodels.SortingFields.SortingField
import datamodels.requests.GetCarListRequest
import datamodels.responses.DeleteCarResponses
import datamodels.responses.DeleteCarResponses.DeleteCarError
import datamodels.responses.GetCarsStatisticResponses.CarsStatistic
import datamodels.responses.PutCarResponses.CarAlreadyExists
import datamodels.responses.PutCarResponses.PutCarError
import doobie.*
import doobie.implicits.*
import doobie.postgres.*
import doobie.postgres.implicits.*
import org.typelevel.log4cats.Logger

trait CarRepository[F[_]: Sync: Logger] {
  def getCar(carId: UUID): F[Option[Car]]

  def getCars(getCarsRequest: GetCarListRequest): F[List[Car]]

  def putCar(car: Car): F[Either[PutCarError, Unit]]

  def deleteCar(carId: UUID): F[Either[DeleteCarError, Unit]]

  def getStatistic: F[CarsStatistic]
}

object CarRepository {
  def instance(implicit xa: Transactor[IO], logger: Logger[IO]): CarRepository[IO] = new CarRepository[IO] {
    override def getCar(carId: UUID): IO[Option[Car]] = (selectCar ++ fr"where id = $carId" ++ fr"limit 1")
      .query[Car]
      .option
      .transact(xa)

    override def getCars(getCarsRequest: GetCarListRequest): IO[List[Car]] =
      (selectCar ++ getCarsRequest.sortingFields.map(convertSortingField).getOrElse(convertSortingField(SortingFields.CreatedAt))
        ++ fr"limit ${getCarsRequest.limit.getOrElse(20)}"
        ++ fr"offset ${getCarsRequest.offset}")
        .query[Car]
        .to[List]
        .transact(xa)

    override def putCar(car: Car): IO[Either[PutCarError, Unit]] = (fr"insert into cars ($carFields) values"
      ++ fr"(${car.id}, ${car.carType}, ${car.number}, ${car.color}, ${car.manufacturingDate}, ${car.createdAt})")
      .update
      .run
      .map(_ => ())
      .attemptSomeSqlState{
        case sqlstate.class23.UNIQUE_VIOLATION => CarAlreadyExists
      }
      .transact(xa)

    override def deleteCar(carId: UUID): IO[Either[DeleteCarError, Unit]] = sql"delete from cars where id = $carId"
      .update
      .run
      .transact(xa)
      .map{
        case 0 => DeleteCarResponses.CarDontExist(carId).asLeft[Unit]
        case _ => Right[DeleteCarError, Unit](())
      }

    override def getStatistic: IO[CarsStatistic] = { // можно попытаться оптимизировать в одну транзакцию
      val getCount = sql"select count(*) from cars"
        .query[Int]
        .unique
        .transact(xa)

      val getOldestRecord = sql"select id from cars where created_at = (select min(created_at) from cars)"
        .query[UUID]
        .unique
        .transact(xa)

      val getNewestRecord = sql"select id from cars where created_at = (select max(created_at) from cars)"
      .query[UUID]
        .unique
        .transact(xa)
      for {
        count <- getCount
        oldestRecordId <- getOldestRecord
        newestRecordId <- getNewestRecord

      } yield CarsStatistic(count = count, oldestCarId = oldestRecordId, newestCarId = newestRecordId)
    }

    private val carFields = fr"id, car_type, car_number, color, manufacturing_date, created_at"
    private val selectCar = fr"select $carFields from cars"
    private def convertSortingField(sortingFields: SortingField): Fragment = sortingFields match {
      case SortingFields.CreatedAt => fr"order by created_at desc"
      case SortingFields.ManufacturingDate => fr"order by manufacturing_date desc"
      case SortingFields.CarNumber => fr"order by car_number desc"
      case SortingFields.CarType => fr"order by car_type desc"
    }
  }
}

