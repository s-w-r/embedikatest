create table if not exists cars
(
    id uuid primary key,
    car_number varchar not null unique,
    car_type varchar not null,
    color varchar not null,
    manufacturing_date date not null,
    created_at timestamp not null default CURRENT_TIMESTAMP
)