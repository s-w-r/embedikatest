create index if not exists manufacturing_date_index on cars using btree(manufacturing_date);
create index if not exists created_at_index on cars using btree(created_at);